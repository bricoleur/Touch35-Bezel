# Mounting Frame for 3.5" RPi Display
designed with [OpenSCAD](https://openscad.org/)

Made for the common 3.5" Raspberry Pi Touchscreen Display, which is directly stackable to a Raspberry Pi board.
This kind of display is available from several brands. An example may be found [here](http://wiki.sunfounder.cc/index.php?title=3.5_Inch_LCD_Touch_Screen_Monitor_for_Raspberry_Pi) (*external link to SunFounder Wiki*). 

The display connects to the SPI bus, and needs only 10 wires. So it could easily mounted offsite from Raspberry Pi board.  
Thus we have 2 versions of the mounting frame:

* Standalone Display
* Display with Raspberry Pi as backpack

**Frontpanel cutout** is apprx. **89 x 71 mm**, the visible overall frame size is **100 x 75 mm**.  
Clamps assembled with M3 or appropriate self tapping screws.  
Raspberry backpack requires additional M2.5 x 16 screws.  

**Caution**:  
The positions of the **cutout** in frontpanel and the **visible bezel** border are **not symmetrical**!  
(Because there is a 3mm horizontal offset in LCD display module's view port.)  
Please make yourself aware of this, before planning, drilling and cutting your frontpanel etc..  


| Standalone | Raspberry Backpack |
|---|---|
 | ![Assembled view](./png/assembly3-300.png) |  ![Assembled view](./png/assembly4-300.png)    |

### STL Files

| Standalone | Raspberry Backpack | Remarks |
|---|---|---|
| **[Kit](./stl/kit.stl)** | **[Kit](./stl/kit-stack.stl)** | Complete parts kit, bezel, clamps, (cover) |
| **[Frame](./stl/bezel.stl)** | -/- | Bezel, identical for both versions |
| **[Clamps](./stl/clamps.stl)** | **[Clamps](./stl/clamps-stack.stl)** | Set of 2 clamps |
| **[Clamp](./stl/clamp.stl)** | **[Clamp](./stl/clamp-stack.stl)** | Single clamp |
|   -/-    |  **[Cover](./stl/cover.stl)**| Raspberry PCB backside cover |



Add missing STL files  in repository


### License   

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span resource="[_:publisher]" rel="dct:publisher">
    <span property="dct:title">the author</span></span>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">this repository</span>.
<br>This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="DE" about="[_:publisher]">
  Germany</span>.
</p>