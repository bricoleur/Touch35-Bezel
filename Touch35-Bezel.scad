// Touch35-Bezel.scad
/// -----------------------------------------------------------
// Mounting frame for 3.5" Raspberry Pi touch screen
// 
// Designed to mount the screen in a front panel etc.
// when it is connected by flexible cable.
//
// Werner Panocha, January 2022
// -----------------------------------------------------------
//

// Select if the Raspberry shall be stacked on the display
// It will create raisers on the clamps
stacking = true;

// -----------------------------------------------------------
// Select the part to print ...
// -----------------------------------------------------------

assembly();
*bezel();
*printClamps(stacking);
*printClamp(stacking);
*pcbCover();
*kit();




*clamp(); // not for print
*rotate([0,180,0]) clamp(stacking);


// -----------------------------------------------------------
// Parameters
// -----------------------------------------------------------
//
frameWidth = 12;            // Visible frame width
frameZ     = 1;             // Frame thickness

// Inner frame around LCD panel
fwx = 1.2;		// frame width x
fwy = 6;		// frame width y
fh = 6;			// frame height


viewPortY = 50.8;       // Viewport size
viewPortX = 75.5;
viewPortOx = -3;        // Viewport, offset to panel body 
viewPortOy = 0;

// Panel dimensions
panelY = 56;
panelX = 86;
panelZ = 6.5;

// Connector dimensions
connY = 5;
connX = 33.5;
connZ = 13.8;

// Connector offset
connOy = 1.3;
connOx = 7.5;



// Clamp
clampX = 6.4;
clampY = panelY + 2*fwy + 12;

clampW = 3;		        // clamp's wall thickness
clampZ = clampW + 2;    // <<== Adopt to frontpanel thickness <==

// Raspberry
raspiX = 85;
raspiY = 56;            // Raspi PCB Y-size
raspiZ = 15;            // raise
raspiScrewX = 58.0;;
raspiScrewY = 49.1;     // Screw displacement
raspiScrewD = 2.5;

// Frame mounting screws 
screwThread = 3;
screwHole = 3.5;
   
// Mounting screw offsets
mountX1 = 3.5;                      // Align with stacked Raspberry!
mountX2 = mountX1 + raspiScrewX;    // Mounting point with stacking option
mountX3 = panelX - mountX1;


$fn=36; 

// -----------------------------------------------------------
// Helper
// -----------------------------------------------------------
module roundedCube(x,y,z,r){

linear_extrude(height = z) 
union() {
	translate([0+r,0+r,0]) circle(r);
	translate([x-r,0+r,0]) circle(r);
	translate([x-r,y-r,0]) circle(r);
	translate([0+r,y-r,0]) circle(r);

	translate([r,0,0]) square([x-2*r,y]);
	translate([0,r,0]) square([x,y-2*r]);
	}
}

// -----------------------------------------------------------
// Panel (illustration / checking)
// -----------------------------------------------------------
module panel(){
    pcbZ = 1.5;
    
    // LCD
    color("grey") cube([panelX, panelY, panelZ - pcbZ]);
    
    // PCB
    translate([0,0,panelZ - pcbZ])
    color("blue") cube([panelX, panelY, pcbZ]);
    
    // Connector
    translate([connOx,connOy,panelZ])
    color("black") cube([connX, connY, connZ]);
}

// -----------------------------------------------------------
// Front bezel
// -----------------------------------------------------------
module bezel(){
    
    // Viewport offsets
    vpox = (panelX - viewPortX)/2 + viewPortOx;
    vpoy = (panelY - viewPortY)/2 + viewPortOy;
    
    // Frontshield offsets
    fsox = vpox - frameWidth;
    fsoy = vpoy - frameWidth;
    
    // Gap around panel body
	gap = .3;		
    
    
    // Front shield
    translate([0,0,-frameZ])
    difference(){
        
        translate([fsox, fsoy,0])
        roundedCube(viewPortX+ 2*frameWidth, viewPortY + 2*frameWidth, frameZ, 5);
      
        // Viewport punch through
        translate([vpox, vpoy, -5]) roundedCube(viewPortX, viewPortY, 20, .5);  
    }
    
	// Frame around the panel body
	difference(){
		translate([-fwx, -fwy, 0]) 
		roundedCube(panelX + 2*fwx, panelY + 2*fwy, fh, 1);
		
		// Panel room punchout
		translate([-gap, -gap, -0.01]) 
		cube([panelX + 2*gap, panelY+ 2*gap, 10]);
		
		// Screw holes, row #1
		//translate([mountX1, -fwy/2, 0]){
        translate([0, -fwy/2, 0]){
			translate([mountX1,0,0]) cylinder(d=screwThread, h = 10);
			translate([mountX3,0,0]) cylinder(d=screwThread, h = 10);
			
            // Stacking option
			translate([mountX2,0,0]) cylinder(d=screwThread, h = 10);
		}
        // Screw holes, row #2
        translate([0, panelY + fwy/2, 0]){
			translate([mountX1,0,0]) cylinder(d=screwThread, h = 10);
			translate([mountX3,0,0]) cylinder(d=screwThread, h = 10);
			
            // Stacking option
			translate([mountX2,0,0]) cylinder(d=screwThread, h = 10);
		}
	}
}

// -----------------------------------------------------------
// Clamp (bracket)
// -----------------------------------------------------------
module clamp(stacking){

	w = clampW;
    
   // Inset of pads (depends on stacking or not)
    // With stacking, we want this in line with the Raspi mounting screws
   ip = stacking ? (panelY - raspiScrewY)/2 : 10;
  
   
    y0 = (clampY - panelY)/2;   // Reference point
   
    
	difference(){
		union(){
			// Bar
			translate([0,0,clampZ]) cube([clampX, clampY, w]);
			// Outer pads
			cube([clampX, w, clampZ]);
			translate([0,clampY-w,0]) cube([clampX, w, clampZ]);
            
            // Inner pads
            translate([0,  y0 + ip, clampZ]) rotate([0,90,0])
            cylinder(d=w, h=clampX);
           
            off = stacking ? ip + raspiScrewY : panelY - ip;
            translate([0,  y0 + off, clampZ]) rotate([0,90,0])
            cylinder(d=w, h=clampX);
            
            // Raiser
            if(stacking){
                translate([clampX/2, y0+ip, clampZ]){
                    hull(){
                        translate([-clampX/2,-clampX/2,0])  cube([clampX,clampX,raspiZ-5]);
                        cylinder(d=5, h=raspiZ);
                    }
                }
                translate([clampX/2, y0+ip + raspiScrewY, clampZ]){
                    hull(){
                        translate([-clampX/2,-clampX/2,0])  cube([clampX,clampX,raspiZ-5]);
                        cylinder(d=5, h=raspiZ);
                    }
                }
            }
		}
		
		// Holes
		translate([clampX/2, (clampY - panelY - fwy)/2, 0]){
			cylinder(d=screwHole, h=10);
			translate([0, panelY + fwy, 0]) cylinder(d=screwHole, h=10);
		}
        
        // Holes in Raiser
        if(stacking){
            translate([clampX/2, y0+ip, clampZ + 6])
            cylinder(d=raspiScrewD, h=10);
            translate([clampX/2, y0+ip+ raspiScrewY, clampZ + 6])
            cylinder(d=raspiScrewD, h=10);
        }
        
	}
}

// -----------------------------------------------------------
// Clamp(s) in print position
// -----------------------------------------------------------
module printClamp(stacking){
    if(stacking)
        translate([0,0,clampX]) rotate([0,90,0]) clamp(stacking);
    else
        translate([clampX,0,clampZ + clampW]) rotate([0,180,0]) clamp(stacking);
}

module printClamps(stacking){
    
    xo = stacking ? 20 : 10;
    
    printClamp(stacking);
    translate([xo,0,0]) printClamp(stacking);
}


// -----------------------------------------------------------
// Just for fun and illustration ...
// -----------------------------------------------------------
module assembly(){
    
    cutX = 90;  // mounting cutout
    cutY =69.5;
    
    // Frontpanel / enclosure etc.
    %translate([0,0,3])
    difference(){
        o = 30;
        g=.5;
        
        translate([-fwx-g-o/2, -fwy-g-o/2, -.1]) cube([cutX+o, cutY+o, 1.5]); 
        translate([-fwx-g,-fwy-g, -.2]) cube([cutX, cutY, 5]); 
    }
    
    // Front bezel 
	bezel();
	
    // LCD panel
	translate([0,0,10]) panel();
	
	// Clamps
    cz = 20;
	yo = (clampY - panelY)/2;
	translate([mountX1 - clampX/2, -yo, cz]) clamp(stacking);
    if(stacking)
        translate([mountX2 - clampX/2, -yo, cz]) clamp(stacking);
    else
        translate([mountX3 - clampX/2, -yo, cz]) clamp(stacking);


    if(stacking)
        translate([0,raspiY,50]) rotate([180,0,0]) pcbCover();
}


// -----------------------------------------------------------
//  Part assembly to print a kit
// -----------------------------------------------------------
module kit(){
	translate([0,0,frameZ]) bezel();
    translate([0,-15,0]) rotate([0,0, -90])  printClamps(stacking);
    if(stacking)
        translate([0,raspiY + 15 ,0]) pcbCover();
}


// -----------------------------------------------------------
//  Shield to cover Raspberry PCB backplane
// -----------------------------------------------------------
module pcbCover(){
	gap = .5;
	wall = 1.2;
	ri = 2;
	bc = 2.5;		// bottom clearance
	pcb = 1.5;
	si = 3;			// screw inset
	
	h = bc + pcb + wall;
	
	bd = 5;		    // base diameter
	bh = bc;
	
	so =  (raspiY - raspiScrewY)/2;		// Hull offset to PCB screws
	
	
	difference(){
		union(){
			difference(){
				// Outer hull
				translate([-(gap+wall), -(gap+wall), 0])
				roundedCube(raspiX + 2*gap + 2*wall, raspiY + 2*gap + 2*wall, h, ri+wall);
				
				// PCB
				translate([-gap, -gap,wall])
				roundedCube(raspiX + 2*gap, raspiY + 2*gap, 5, ri);
			}
			
			// Spacers
			translate([so, so, wall]) {
				translate([0,0,0]) cylinder(d=bd, h=bh);
				translate([raspiScrewX,0,0]) cylinder(d=bd, h=bh);
				translate([0,raspiScrewY,0]) cylinder(d=bd, h=bh);
				translate([raspiScrewX,raspiScrewY,0]) cylinder(d=bd, h=bh);
			}
		}
	
		// Bore
		bbh = bh + 10;
		bbd = 3;
		translate([so, so, -1]) {
				translate([0,0,0]) cylinder(d=bbd, h=bbh);
				translate([raspiScrewX,0,0]) cylinder(d=bbd, h=bbh);
				translate([0,raspiScrewY,0]) cylinder(d=bbd, h=bbh);
				translate([raspiScrewX,raspiScrewY,0]) cylinder(d=bbd, h=bbh);
		}
        
        // SD card access
        sdy = 13;
        sdx=5;
        translate([-(sdx + gap - 0.01), raspiY/2 - sdy/2,-.1]) cube([sdx, sdy, 10]);
		
	}
}

// eof
